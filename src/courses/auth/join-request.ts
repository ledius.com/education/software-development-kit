import { Type } from 'class-transformer';

export class JoinRequest {
  @Type(() => String)
  public email: string;

  @Type(() => String)
  public phone: string;

  static create(email: string, phone: string): JoinRequest {
    const self = new JoinRequest();
    self.email = email;
    self.phone = phone;
    return self;
  }
}
