export class JoinResponse {
  public id: string;
  public email: string;
  public phone: string;

  static create(data: JoinResponse): JoinResponse {
    const self = new JoinResponse();
    self.id = data.id;
    self.email = data.email;
    self.phone = data.phone;
    return self;
  }
}
