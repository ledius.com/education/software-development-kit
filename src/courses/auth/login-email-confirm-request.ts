import { Type } from 'class-transformer';

export class LoginEmailConfirmRequest {

  @Type(() => String)
  public email: string;

  @Type(() => String)
  public code: string;

  static create(email: string, code: string): LoginEmailConfirmRequest {
    const self = new LoginEmailConfirmRequest();
    self.email = email;
    self.code = code;
    return self;
  }
}
