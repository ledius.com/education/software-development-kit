import { Type } from 'class-transformer';

export class TokenRefreshRequest {
  @Type(() => String)
  public refresh: string;

  static create(refresh: string): TokenRefreshRequest {
    const self = new TokenRefreshRequest();
    self.refresh = refresh;
    return self;
  }
}
