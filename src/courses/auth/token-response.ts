import { Type } from 'class-transformer';

export class TokenResponse {
  @Type(() => String)
  public readonly accessToken: string;

  @Type(() => String)
  public readonly refreshToken: string;

  @Type(() => String)
  public readonly type: string;

  @Type(() => Date)
  public readonly expiresIn: Date;
}
