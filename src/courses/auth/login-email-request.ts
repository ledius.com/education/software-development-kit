import { Type } from 'class-transformer';

export class LoginEmailRequest {

  @Type(() => String)
  public email: string;

  public static create(email: string) {
    const self = new LoginEmailRequest();
    self.email = email;
    return self;
  }
}
