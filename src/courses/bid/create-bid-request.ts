import { Type } from 'class-transformer';

export class CreateBidRequest {
  public userId!: string;
  public consultationId!: string;
  @Type(() => Number)
  public price!: number;
  public description!: string;

  public static create(request: CreateBidRequest): CreateBidRequest {
    const self = new CreateBidRequest();
    self.userId = request.userId;
    self.consultationId = request.consultationId;
    self.description = request.description;
    self.price = request.price;
    return self;
  }
}
