import { ConsultationResponse } from '../consultation';
import { Status } from './status';
import { Type } from 'class-transformer';

export class BidResponse {
  public id: string;

  public userId: string;

  @Type(() => ConsultationResponse)

  public consultation: ConsultationResponse;

  public description: string;

  public price: number;

  public status: Status;

  @Type(() => Date)

  public createdAt: Date;

  public static create(bid: BidResponse): BidResponse {
    const self = new BidResponse();
    self.id = bid.id;
    self.price = bid.price;
    self.status = bid.status;
    self.consultation = bid.consultation;
    self.userId = bid.userId;
    self.description = bid.description;
    self.createdAt = bid.createdAt;
    return self;
  }
}
