import { Status } from './status';

export class ConsultationResponse {
  public id: string;

  public content: {
    name: string;
    description: string;
    icon: string;
  };

  public price: number;

  public status: Status;

  public static create(consultation: ConsultationResponse): ConsultationResponse {
    const self = new ConsultationResponse();
    self.id = consultation.id
    self.content = consultation.content;
    self.price = consultation.price;
    self.status = consultation.status;
    return self
  }
}
